require 'squib'
require 'pp'
require_relative 'helpers' 

data = Squib.csv file: 'data/akro-mills-small.csv'

bottomline = []
data.line_1.each_with_index do |str, idx|
    line = ''
    line += str unless str.nil?
    line2 = data.row(idx)['Line 2']

    line += "\n#{line2}" unless line2.nil?
    bottomline << unsplit_x_by_x(line)
end

color = data.color.map {|c| color_for_spec(c)}

Squib::Deck.new(width: '49mm', height: '66mm', cards: data.nrows ) do
    use_layout file: 'layouts/akro-mills.yml'

    rect x: 0, y: 0, width: '50mm', height: '12mm', fill_color: color, stroke_color: '#0000'
    svg file: 'logos-49mm.svg', y: 0

    text str: data['Catagory'], x:0, y: '12mm', width: '49mm', height: '8mm', layout: :bigfont

    text str: bottomline, x:0, y: '20mm', width: '49mm', height: '13mm', layout: :smallfont
   
    #fold line
    line x1: 0, y1: '33mm', x2: '49mm', y2: '33mm', stroke_color: '#aaa', dash: '0.02in 0.02in'

    save_pdf file: 'akro-mills_small.pdf', crop_marks: true, margin: '0.5in'

end
