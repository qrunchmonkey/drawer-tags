def unsplit_x_by_x(str)
    # \u00A0x is a non-breaking space
    str.gsub(/(\d+(?:"|'|mm|in)?)\s+x\s+(\d+)/, "\\1\u00A0x\u00A0\\2")
end

def color_for_spec(spec)
    case spec.downcase
    when 'metric'
        "#f00"
    when 'imperial'
        "#00f"
    when 'stock'
        "#fb0"
    else
        "#f0f"
    end
end