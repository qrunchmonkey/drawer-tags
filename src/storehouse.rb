require 'squib'
require 'pp'
require_relative 'helpers' 

data = Squib.csv file: 'data/storehouse.csv'

bottomline = []
data.line_1.each_with_index do |str, idx|
    line = ''
    line += str
    line2 = data.row(idx)['Line 2']

    line += "\n#{line2}" unless line2.nil?
    bottomline << unsplit_x_by_x(line)
end

color = data.color.map {|c| color_for_spec(c)}

Squib::Deck.new(width: '45mm', height: '60mm', cards: data.nrows ) do
    use_layout file: 'layouts/storehouse.yml'


    text str: data['Catagory'], x:0, y:0, width: '45mm', height: '9.5mm', layout: :bigfont

    text str: bottomline, x:0, y: '17.5mm', width: '45mm', height: '12.5mm', layout: :smallfont

    rect x: 0, y: '9.5mm', width: '45mm', height: '8mm', fill_color: color, stroke_color: '#0000'
    svg file: 'logos-45mm.svg', y: '9.5mm'
    #fold line
    line x1: 0, y1: '30mm', x2: '45mm', y2: '30mm', stroke_color: '#aaa', dash: '0.02in 0.02in'

    save_pdf file: 'storehouse.pdf', crop_marks: true, margin: '0.5in'

end
